import java.util.EventObject;

public class FormEvent extends EventObject {

	private String imie;
	private String nazwisko;
	
	public FormEvent(Object source) {
		super(source);
	}
	
	public FormEvent(Object source, String imie, String nazwisko) {
		super(source);
		
		this.imie = imie;
		this.nazwisko = nazwisko;
	}
	
	public String getImie() {
		return imie;
	}
	public void setImie(String imie) {
		this.imie = imie;
	}
	public String getNazwisko() {
		return nazwisko;
	}
	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}
}
