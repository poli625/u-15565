import java.awt.FlowLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;

public class Toolbar extends JPanel implements ActionListener {
	
	private JButton buttonone;
	private JButton buttontwo;
	
	private StringListener textListener;
	
	public Toolbar() {
		buttonone = new JButton("Yo");
		buttontwo = new JButton("Bye");
		
		buttonone.addActionListener(this);
		buttontwo.addActionListener(this);
		
		setLayout(new FlowLayout(FlowLayout.LEFT));
		
		add(buttonone);
		add(buttontwo);
	}
	
	public void setStringListener(StringListener listener) {
		this.textListener = listener;
	}
	
	public void actionPerformed(ActionEvent e) {
		JButton clicked = (JButton)e.getSource();
		
		if(clicked == buttonone) {
			if(textListener != null) {
				textListener.textEmitted("One\n");
			}
		}else if(clicked == buttontwo) {
			if(textListener != null) {
				textListener.textEmitted("Two\n");
			}
		}
	}
}
