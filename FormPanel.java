import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;

public class FormPanel extends JPanel {
	
	private JLabel imieLabel;
	private JLabel nazwiskoLabel;
	private JTextField imieTField;
	private JTextField nazwiskoTField;
	private JButton registerbtn;
	private FormListener formListener;
	private JList ageList;
	
	public FormPanel() {
		Dimension dim = getPreferredSize();
		dim.width = 250;
		setPreferredSize(dim);
		
		imieLabel = new JLabel("Imi� : ");
		nazwiskoLabel = new JLabel("Nazwisko : ");
		imieTField = new JTextField(10);
		nazwiskoTField = new JTextField(10);
		registerbtn = new JButton("Akceptuj");
		ageList = new JList();
		
		DefaultListModel ageModel = new DefaultListModel();
		ageModel.addElement("Poni�ej 18");
		ageModel.addElement("18 do 65");
		ageModel.addElement("Poni�ej 65 albo powy�ej");
		ageList.setModel(ageModel);
		
		ageList.setPreferredSize(new Dimension(150, 66));
		ageList.setBorder(BorderFactory.createEtchedBorder());
		ageList.setSelectedIndex(1);
		
		registerbtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String imie = imieTField.getText();
				String nazwisko = nazwiskoTField.getText();
				String wiekKate = (String)ageList.getSelectedValue();
				
				System.out.println(wiekKate);
				
				FormEvent ev = new FormEvent(this, imie, nazwisko);
				
				if(formListener != null) {
					formListener.formEventOccurred(ev);
				}
			}
		});
		
		Border innerBorder = BorderFactory.createTitledBorder("Dodaj osob�");
		Border outerBorder = BorderFactory.createEmptyBorder();
		setBorder(BorderFactory.createCompoundBorder(innerBorder, outerBorder));
		
		setLayout(new GridBagLayout());
		
		GridBagConstraints gc = new GridBagConstraints();
		
		//First row//
		
		gc.weightx = 1;
		gc.weighty = 0.1;
		
		gc.gridx = 0;
		gc.gridy = 0;
		gc.fill = GridBagConstraints.NONE;
		gc.anchor = GridBagConstraints.LINE_END;
		gc.insets = new Insets(0,0,0,5);
		
		add(imieLabel, gc);
		
		gc.gridx = 1;
		gc.gridy = 0;
		gc.anchor = GridBagConstraints.LINE_START;
		gc.insets = new Insets(0,0,0,0);
		
		add(imieTField, gc);
	
		//Second row
		
		gc.weightx = 1;
		gc.weighty = 0.1;
		
		gc.gridx = 0;
		gc.gridy = 1;
		gc.anchor = GridBagConstraints.LINE_END;
		gc.insets = new Insets(0,0,0,5);
		
		add(nazwiskoLabel, gc);
		
		gc.gridx = 1;
		gc.gridy = 1;
		gc.anchor = GridBagConstraints.LINE_START;
		gc.insets = new Insets(0,0,0,0);
		
		add(nazwiskoTField, gc);
		
		//Third row
		
		gc.weightx = 1;
		gc.weighty = 0.2;
		gc.gridx = 1;
		gc.gridy = 2;
		gc.anchor = GridBagConstraints.FIRST_LINE_START;
		gc.insets = new Insets(0,0,0,0);
		
		add(ageList,gc);
		
		//Fourth row
		
		gc.weightx = 1;
		gc.weighty = 2.0;
		gc.gridx = 1;
		gc.gridy = 3;
		gc.anchor = GridBagConstraints.FIRST_LINE_START;
		gc.insets = new Insets(0,0,0,0);
		
		add(registerbtn,gc);
	}
	
	public void setFormListener(FormListener listener) {
		this.formListener = listener;
	}
}
